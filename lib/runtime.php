<?php

function dlog($format) {
	global $log;
	if ( !$log ) return;
	
	$argv = func_get_args ();
	reset ( $argv );
	for ( $i = 0; $i < count ( $argv ); $i++ ) {
		if ( is_array ( $argv[ $i ] ) ) {
			$argv[ $i ] = print_r ( $argv[ $i ], true );
		} else {
			if ( is_object ( $argv[ $i ] ) ) {
				$argv[ $i ] = sprintf ( "<%s: %s>", get_class ( $argv[ $i ] ), spl_object_hash ( $argv[ $i ] ) );
			}
		}
	}
	
	$caller = array_shift ( debug_backtrace () );
	$time = microtime ( true );
	$msg = sprintf ( "%s.%d %s[%4s] %s\n", date ( "Y-n-d H:i:s" ), round ( ($time - floor ( $time )) * 1000 ), pathinfo ( $caller[ 'file' ], PATHINFO_FILENAME ), $caller[ 'line' ], call_user_func_array ( 'sprintf', $argv ) );
	$file = fopen ( 'hook.log', 'a+' );
	fwrite ( $file, $msg );
	fclose ( $file );
}