<?php

/**
 *
 * @param string $payload        	
 * @return array
 */
function bitbucketParsePayload($payload) {
	$info = array ( 
		'repo' => $payload[ 'repository' ][ 'name' ],
		'path' => preg_replace ( array ( 
			"/^\//",
			"/\/$/" 
		), array ( 
			"",
			"" 
		), $payload[ 'repository' ][ 'absolute_url' ] ),
		'url' => $payload[ 'canon_url' ] . $payload[ 'repository' ][ 'absolute_url' ],
		'commits' => array () 
	);
	
	foreach ( $payload[ 'commits' ] as $commit ) {
		$info[ 'commits' ][] = array ( 
			'author' => $commit[ 'author' ],
			'node' => $commit[ 'raw_node' ],
			'url' => $info[ 'url' ] . 'commits/' . $commit[ 'raw_node' ],
			'message' => trim(strip_tags($commit[ 'message' ])),
			'branch' => $commit[ 'branch' ] 
		);
	}
	
	return $info;
}
