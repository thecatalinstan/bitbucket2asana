<?php
define ( 'taskid_pattern', '/#([0-9]+)/' ); // regex pattern to recognize a story number
define ( 'closes_pattern', '/([Ff]ix|[Cc]lose|[Cc]losing)/' ); // regex pattern to recognize a "closing this ticket" word
define ( 'and_pattern', '/([Aa]nd|&)/' ); // regex pattern to recognize an "and" word (eg "fixes #1, #2, and #3")

define ( 'asana_close_task_call_format', 'curl --request PUT -u %s: https://app.asana.com/api/1.0/tasks/%s -d "completed=true" > /dev/null 2>&1' );
define ( 'asana_comment_on_task_call_format', 'curl -u %s: https://app.asana.com/api/1.0/tasks/%s/stories -d "text=%s" > /dev/null 2>&1' );

/**
 *
 * @param string $bitbucketUsername        	
 *
 * @return string boolean
 */
function asanaApiKey($bitbucketUsername) {
	global $apiKeys;
	return isset ( $apiKeys[ $bitbucketUsername ] ) ? $apiKeys[ $bitbucketUsername ] : false;
}

/**
 * 
 * @param array $info
 * @param number $commitIdx
 * @return string
 */
function asanaMakeMessage($info, $commitIdx = 0) {
	$commit = $info[ 'commits' ][ $commitIdx ];
	return trim ( sprintf ( "%s pushed to branch %s of %s (%s)\n\n%s", $commit[ 'author' ], $commit[ 'branch' ], $info[ 'path' ], $commit[ 'url' ], $commit[ 'message' ] ) );
}

function asanaParseMessage( $message ) {
	# break the commit comment down into words
	$words = preg_split("/(?<=\w)\b\s*/", str_replace("\n", "", $message ) );
	
	$referenes = array();
	$closes = array();
	$closing = false;
	
	foreach ( $words as $word ) {
		$matches = array();

		if ( preg_match(taskid_pattern, $word, $matches) ) {
			$referenes[] = $matches[1];
			if ( $closing ) $closes[] = $matches[1];			
		} else if ( preg_match(closes_pattern, $word) ) {
			$closing = true;
		} else if ( ! preg_match(and_pattern, $word) ) {
			$closing = false;
		}
	}
	return array($referenes, $closes, $debug);	
}

function asanaCloseTask ( $task, $key ) {
	return sprintf( asana_close_task_call_format, $key, $task );
} 

function asanaCommentOnTask ( $task, $key, $comment ) {
	return sprintf( asana_comment_on_task_call_format, $key, $task, urlencode($comment) );
}
