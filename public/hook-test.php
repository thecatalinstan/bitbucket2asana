<?php
if ( $_SERVER[ 'REQUEST_METHOD' ] == 'POST' ) {
	include 'hook.php';
	die ();
}
$output = <<< __END__
<!DOCTYPE html><html><head><title>Bitbucket Payload Simulator</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script><script>
function commit() {
	var payload = {"repository": {"website": "", "fork": false, "name": "Bitbucket2Asana", "scm": "git", "owner": "thecatalinstan", "absolute_url": "/thecatalinstan/bitbucket2asana/", "slug": "bitbucket2asana", "is_private": false}, "truncated": false, "commits": [{"node": "beed5dfe194d", "files": [{"type": "modified", "file": "public/hook.php"}], "raw_author": "Catalin Stan <catalin.stan@me.com>", "utctimestamp": "2014-04-10 17:40:57+00:00", "author": "thecatalinstan", "timestamp": "2014-04-10 19:40:57", "raw_node": "beed5dfe194df9f4e795c8c8b6f2429ec52b0f5a", "parents": ["8e39471dadea"], "branch": "master", "message": "Signed-off-by: Catalin Stan <catalin.stan@me.com>", "revision": null, "size": -1}], "canon_url": "https://bitbucket.org", "user": "thecatalinstan"};
	payload.commits[0].message = $('#message').text();
	$.ajax({url : "hook-test.php",data : {payload :JSON.stringify( payload )},type : "POST"}).done(function(data) { $('#output').html(data.output); $('#payload').html(data.payload); });
}
$(document).ready(function() { $('#send').click(function() { commit(); }); });</script><style>
body, html { font-family: "Helvetica Neue", Helvetica, sans-serif; font-weight: 200; font-size: 10px/1.2em; margin: 0; padding: 0; margin: 0; }
#content { width: 780px; margin: 0 auto; }
section { border-bottom: 1px dotted #ccc; padding: 0 0 20px 0; }
#message { padding: 5px; border-radius: 5px; border: 1px solid #ccc; min-height: 100px; margin-bottom: 5px }
.result { overflow: auto; padding: 5px; border-radius: 5px; min-height: 100px; margin-bottom: 5px; background-color: #eee; }
p.label { margin-bottom: 5px; }
</style></head><body>
<div id="content"><h1>Bitbucket Payload Simulator</h1>
<section id="commitmessage"><p class="label">Enter a commit message:</p><div id="message" contenteditable="true"></div><input id="send" type="submit" value="Send Commit Message" /></section>
<section id="params"><h2>Output</h2><div class="result" id="output"></div><h2>Payload</h2><div class="result" id="payload"></div></section>
</div></body></html>
__END__;
echo $output;