<?php
require_once '../etc/config.php';
require_once '../lib/runtime.php';
require_once '../lib/bitbucket.php';
require_once '../lib/asana.php';

// valid requests are with post and have a payload
if ( $_SERVER[ 'REQUEST_METHOD' ] != 'POST' ) {
	header ( 'Content-type: text/plain', true, 405 );
	header ( 'Allow: POST' );
	dlog ( "%s", "Invalid request method" );
	die ();
}

// valid payloads have a repository key ...
// TODO: Add some other validations for payload
if ( !isset ( $_POST[ 'payload' ] ) ) {
	header ( 'Content-type: text/plain', true, 415 );
	dlog ( "%s", "Invalid media type" );
	die ();
}

// Testing mode (hook.php is included in a php file that has 'test' in its name)
$testing = __FILE__ != $_SERVER[ 'SCRIPT_FILENAME' ] && strstr ( $_SERVER[ 'SCRIPT_FILENAME' ], "test" ) !== 0;

// If we're testing we return a JSON object
header ( 'Content-type: ' . ($testing ? 'application/json' : 'text/plain') . '; charset=utf-8', true );

// Parse the payload and generate an array
$payload = json_decode ( $_POST[ 'payload' ], true );
if ( !$payload || !isset ( $payload[ 'repository' ] ) ) {
	header ( 'Content-type: text/plain', true, 415 );
	dlog ( "%s", $payload );
	die ();
}

$info = bitbucketParsePayload ( $payload );
$calls = array ();
for ( $i = 0; $i < count ( $info[ 'commits' ] ); $i++ ) {
	$commit = $info[ 'commits' ][ $i ];
	
	// generate the comment
	$commit[ 'comment' ] = asanaMakeMessage ( $info, $i );
	
	// get the user who posts the comment
	$commit[ 'apikey' ] = asanaApiKey ( $commit[ 'author' ] );
	
	// parse the commit message and get the
	$commit[ 'references' ] = array ();
	$commit[ 'closes' ] = array ();
	
	// parse tags
	list ( $commit[ 'references' ], $commit[ 'closes' ] ) = asanaParseMessage ( $commit[ 'message' ] );
	
	// get a list of calls for comments
	foreach ( $commit[ 'references' ] as $task ) {
		if ( !is_array ( $calls[ $task ] ) ) $calls[ $task ] = array ();
		$calls[ $task ][] = asanaCommentOnTask ( $task, $commit[ 'apikey' ], $commit[ 'comment' ] );
	}
	
	// get a list of calls for closing tasks
	foreach ( $commit[ 'closes' ] as $task ) {
		if ( !is_array ( $calls[ $task ] ) ) $calls[ $task ] = array ();
		$calls[ $task ][] = asanaCloseTask ( $task, $commit[ 'apikey' ] );
	}
	
	$info[ 'commits' ][ $i ] = $commit;
}

dlog ( "%s", $info );
dlog ( "%s", $calls );

if ( !$testing ) {
	// call the api
	reset ( $calls );
	foreach ( $calls as $task => $taskCalls ) {
		foreach ( $taskCalls as $call ) {
			exec ( $call );
			dlog ( "%s", $call );
		}
	}
	exit ();
}

header ( 'Content-type: application/json; charset=utf-8', true );
$output = array ( 
	'testing' => var_export ( $testing, true ) 
);

$output[ 'info' ] = $info;
$output[ 'calls' ] = $calls;

echo json_encode ( array ( 
	'payload' => '<pre>' . print_r ( $payload, true ) . '</pre>',
	'output' => '<pre>' . print_r ( $output, true ) . '</pre>' 
) );
